#include <stdio.h>
#include <math.h>
#include <stdlib.h>

typedef struct Vector_tag {

    int N;
    float* data;

} Vector;

int allocate(Vector* u, int N){

    if (N < 0) return 1;

    u->data = malloc(N*sizeof(float));
    if (u->data == NULL)
        return 1;

    u->N = N;

    return 0;
}

int deallocate(Vector* u){

    free(u->data);

    u->data = NULL;
    u->N = 0;

    return 0;
}

int initialize(Vector* u){

    if(u->data == NULL) return 1;

    for (int i = 0; i < u->N; ++i){
        u->data[i] = 0.0;
    }

    return 0;
}

int inner_product(Vector* u, Vector* v, float* sum){

    *sum = 0.0;

    if(u->data == NULL) return 1;
    if(v->data == NULL) return 1;
    if (u->N != v->N) return 1;

    for (int i = 0; i < u->N; ++i){
        *sum += u->data[i]*v->data[i];
    }

    return 0;
}

float norm(Vector* u){

    float ip;
    inner_product(u, u, &ip);
    return sqrt(ip);
}

int normalize(Vector* u){

    float len = norm(u);

    if(u->data == NULL) return 1;
    if (len < 1e-7)
        return 1;

    for(int i = 0; i < u->N; ++i){
        u->data[i] /= len;
    }

    return 0;
}


int axpy(float a, Vector* u, Vector* v, Vector* w){

    if (u->N != v->N) return 1;
    if (u->N != w->N) return 1;
    if(u->data == NULL) return 1;
    if(v->data == NULL) return 1;
    if(w->data == NULL) return 1;
    for (int i = 0; i < u->N; ++i){
        w->data[i] = a*u->data[i] + v->data[i];
    }

    return 0;
}

void print(Vector* u){

    printf("[");
    for(int i = 0; i < u->N-1; ++i){
        printf("%f, ", u->data[i]);
    }
    printf("%f]", u->data[u->N-1]);
}
