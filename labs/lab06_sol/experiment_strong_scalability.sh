#!/bin/bash

echo "strong scalability"
echo "=================="
echo "experiment, n_procs, n_trials, N, avg_time" > strong.csv
mpirun -np 1 ./time_distributed_axpy 10000000 5 >> strong.csv
mpirun -np 2 ./time_distributed_axpy 10000000 5 >> strong.csv
mpirun -np 4 ./time_distributed_axpy 10000000 5 >> strong.csv
mpirun -np 8 ./time_distributed_axpy 10000000 5 >> strong.csv
mpirun -np 16 ./time_distributed_axpy 10000000 5 >> strong.csv
mpirun -np 32 ./time_distributed_axpy 10000000 5 >> strong.csv
mpirun -np 64 ./time_distributed_axpy 10000000 5 >> strong.csv
mpirun -np 128 ./time_distributed_axpy 10000000 5 >> strong.csv
mpirun -np 256 ./time_distributed_axpy 10000000 5 >> strong.csv

echo ""
echo ""