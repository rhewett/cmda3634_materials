#!/bin/bash

echo "weak scalability"
echo "=================="
echo "experiment, n_procs, n_trials, N, avg_time" > weak.csv
mpirun -np 1 ./time_distributed_axpy 1000000 5 >> weak.csv
mpirun -np 2 ./time_distributed_axpy 2000000 5 >> weak.csv
mpirun -np 4 ./time_distributed_axpy 4000000 5 >> weak.csv
mpirun -np 8 ./time_distributed_axpy 8000000 5 >> weak.csv
mpirun -np 16 ./time_distributed_axpy 16000000 5 >> weak.csv
mpirun -np 32 ./time_distributed_axpy 32000000 5 >> weak.csv
mpirun -np 64 ./time_distributed_axpy 64000000 5 >> weak.csv
mpirun -np 128 ./time_distributed_axpy 128000000 5 >> weak.csv
mpirun -np 256 ./time_distributed_axpy 256000000 5 >> weak.csv

echo ""
echo ""
