/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#ifndef __PARALLEL_ARRAY_IO_H__
#define __PARALLEL_ARRAY_IO_H__

int read_simple_array_format_dist_cio(float** arr,
                                      unsigned int* nr_local, unsigned int* nc_local,
                                      unsigned int* nr_padded, unsigned int* nc_padded,
                                      unsigned int* nr_global, unsigned int* nc_global,
                                      unsigned int padding_nr, unsigned int padding_nc,
                                      MPI_Comm comm,
                                      char* filename);

int write_simple_array_format_dist_cio(float* arr,
                                       unsigned int nr_local, unsigned int nc_local,
                                       unsigned int nr_padded, unsigned int nc_padded,
                                       unsigned int nr_global, unsigned int nc_global,
                                       unsigned int padding_nr, unsigned int padding_nc,
                                       MPI_Comm comm,
                                       char* filename);

int write_simple_array_format_dist_mpiio(float* arr,
                                         unsigned int nr_local, unsigned int nc_local,
                                         unsigned int nr_padded, unsigned int nc_padded,
                                         unsigned int nr_global, unsigned int nc_global,
                                         unsigned int padding_nr, unsigned int padding_nc,
                                         MPI_Comm comm,
                                         char* filename);


#endif