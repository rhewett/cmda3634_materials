#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <mpi.h>

#include "parallel_array_io.h"

/*
* Helper function for computing a rank's subarray size.
*
* Arguments:
*   N: integer length of a particular dimension
*   P: total processors
*   rank: rank of the processor to get the subarray size for
*
* Returns:
*   subarray size
*/

unsigned int subarray_size(unsigned int N, unsigned int P, unsigned int rank){

//    <student>

    printf("You should remove this line!  If you see it in your output you didn't finish this function first!\n")

//    </student>

}



/*
* Reads an nr_global x nc_global x 4 RGBA array from disk, using C-style IO
*
* Note: Acts as an allocator for the grid.
*
* Arguments:
*   arr: The data array pointer to read to
*   nr_local: number of rows in the array for this rank, without padding
*   nc_local: number of columns in the array for this rank, without padding
*   nr_padded: number of rows in the array for this rank, with padding
*   nc_padded: number of columns in the array for this rank, with padding
*   nr_global: number of rows in the grid, total over all ranks
*   nc_global: number of columns in the grid, total over all ranks
*   padding_nr: number of padding rows in the array for this rank
*   padding_nc: number of padding columns in the array for this rank
*   comm: MPI communicator the grid is distributed over
*   filename: string name of the file to read
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int read_simple_array_format_dist_cio(float** arr,
                                      unsigned int* nr_local, unsigned int* nc_local,
                                      unsigned int* nr_padded, unsigned int* nc_padded,
                                      unsigned int* nr_global, unsigned int* nc_global,
                                      unsigned int padding_nr, unsigned int padding_nc,
                                      MPI_Comm comm,
                                      char* filename){

    // File handle
    FILE* f;

    // Total data to write, measured data written
    unsigned int n_data, n_read, n_data_padded;
    unsigned int nchannels = 4;

    int rank, size;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    unsigned int channels;
    // Only rank 0 should manage file creation and write the header
    if (rank == 0){
        // Open the file
        f = fopen(filename, "rb");
        if (f == NULL) {
            fprintf(stderr, "Error opening file %s for read.\n", filename);
            return 1;
        }

        // Write the header
        n_read = fread(nr_global, sizeof(unsigned int), 1, f);
        if (n_read != 1){
            fprintf(stderr, "Error reading nr from file %s: %d of 1 data read.\n", filename, n_read);
            return 1;
        }

        n_read = fread(nc_global, sizeof(unsigned int), 1, f);
        if (n_read != 1){
            fprintf(stderr, "Error reading nc from file %s: %d of 1 data read.\n", filename, n_read);
            return 1;
        }

        n_read = fread(&channels, sizeof(unsigned int), 1, f);
        if (n_read != 1){
            fprintf(stderr, "Error reading channels from file %s: %d of 1 data read.\n", filename, n_read);
            return 1;
        }
    }

    MPI_Bcast(nr_global, 1, MPI_UNSIGNED, 0, comm);
    MPI_Bcast(nc_global, 1, MPI_UNSIGNED, 0, comm);
    MPI_Bcast(&channels, 1, MPI_UNSIGNED, 0, comm);

    // Get the subsizes in the row and column dimensions
    *nr_local = subarray_size(*nr_global, size, rank);
    *nc_local = subarray_size(*nc_global, 1, rank);
    *nr_padded = *nr_local + 2*padding_nr;
    *nc_padded = *nc_local + 2*padding_nc;

    // Total data we will store
    n_data_padded = (*nr_padded) * (*nc_padded) * nchannels;
    *arr = malloc(n_data_padded*sizeof(float));

    if (*arr == NULL){
        fprintf(stderr, "Error allocating RGBA array.\n");
        return 1;
    }

    // The total data this rank will be reading
    n_data = (*nr_local) * (*nc_local) * nchannels;

    // This is a little inefficient, but for simplicity we will create a local,
    // unpadded arrays so we can do a single communication.  This is safe because
    // by specification, Rank 0 has the largest possible array.
    float* read_data = (float*) malloc(n_data*sizeof(float));
    if(read_data == NULL){
        fprintf(stderr, "Error allocating temporary array data.\n");
        return 1;
    }
    memset(read_data, 0, n_data*sizeof(float));

    if (rank == 0){

        // Rank 0 can directly read its own data
        n_data = (*nr_local) * (*nc_local) * nchannels;
        n_read = fread(read_data, sizeof(float), n_data, f);
        if(n_read != n_data){
            fprintf(stderr, "Error reading file %s:expected %d bytes read %d bytes.\n", filename, n_data, n_read);
            return 1;
        }

        // Loop over the local, non-padded part of the data and fill
        // with the data
        for(int i=0;i<*nr_local;i++){

            // Copy the ith row into the ith row of read_data.
            // The ith row starts padding_nr rows into the provided array
            // and the data we care about starts padding_nc columns into
            // that row.
            // The length of the row is nc_local.
            memcpy(*arr + (padding_nr*(*nc_padded) + i*(*nc_padded) + padding_nc)*nchannels,
                   read_data + i*(*nc_local)*nchannels,
                   (*nc_local)*nchannels*sizeof(float));
        }

        // Rank 0 receives chunks of data from all other processors and writes
        // it to disk
        int nr_p, nc_p;
        for(int p=1; p<size; p++){

            // Determine the remote ranks array properties (local rows and
            // columns, amount of data, etc.)
            nr_p = subarray_size(*nr_global, size, p);
            nc_p = subarray_size(*nc_global, 1, p);
            n_data = nr_p*nc_p*nchannels;

            memset(read_data, 0, n_data*sizeof(float));

            // Rank 0 obtains part of the array from every other rank

            n_read = fread(read_data, sizeof(float), n_data, f);
            if(n_read != n_data){
                fprintf(stderr, "Error reading file %s:expected %d bytes read %d bytes.\n", filename, n_data, n_read);
                return 1;
            }
            MPI_Send(read_data, n_data, MPI_FLOAT, p,  0,  comm);
        }

        fclose(f);
    }
    else{
        // All other ranks just send their respective chunk of the array
        // to rank 0
        MPI_Status status;
        MPI_Recv(read_data, n_data, MPI_FLOAT, 0,  0,  comm, &status);

        // Loop over the local, non-padded part of the data and fill
        // with the data
        for(int i=0;i<*nr_local;i++){

            // Copy the ith row into the ith row of read_data.
            // The ith row starts padding_nr rows into the provided array
            // and the data we care about starts padding_nc columns into
            // that row.
            // The length of the row is nc_local.
            memcpy(*arr + (padding_nr*(*nc_padded) + i*(*nc_padded) + padding_nc)*nchannels,
                   read_data + i*(*nc_local)*nchannels,
                   (*nc_local)*nchannels*sizeof(float));
        }
    }

    free(read_data);

    return 0;

}


/*
* Writes an nr_global x nc_global x 4 RGBA array to disk, using C-style IO
*
* Arguments:
*   arr: The data array pointer to read to
*   nr_local: number of rows in the array for this rank, without padding
*   nc_local: number of columns in the array for this rank, without padding
*   nr_padded: number of rows in the array for this rank, with padding
*   nc_padded: number of columns in the array for this rank, with padding
*   nr_global: number of rows in the grid, total over all ranks
*   nc_global: number of columns in the grid, total over all ranks
*   padding_nr: number of padding rows in the array for this rank
*   padding_nc: number of padding columns in the array for this rank
*   comm: MPI communicator the grid is distributed over
*   filename: string name of the file to read
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int write_simple_array_format_dist_cio(float* arr,
                                       unsigned int nr_local, unsigned int nc_local,
                                       unsigned int nr_padded, unsigned int nc_padded,
                                       unsigned int nr_global, unsigned int nc_global,
                                       unsigned int padding_nr, unsigned int padding_nc,
                                       MPI_Comm comm,
                                       char* filename){

    // File handle
    FILE* f;

    // Total data to write, measured data written
    unsigned int n_data, n_written;
    unsigned int nchannels = 4;

    int rank, size;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    // Only rank 0 should manage file creation and write the header
    if (rank == 0){
        // Open the file
        f = fopen(filename, "wb");
        if (f == NULL) {
            fprintf(stderr, "Error opening file %s for write.\n", filename);
            return 1;
        }

        // Write the header
        n_written = fwrite(&nr_global, sizeof(unsigned int), 1, f);
        if (n_written != 1){
            fprintf(stderr, "Error writing nr to file %s: %d of 1 data written.\n", filename, n_written);
            return 1;
        }

        n_written = fwrite(&nc_global, sizeof(unsigned int), 1, f);
        if (n_written != 1){
            fprintf(stderr, "Error writing nc to file %s: %d of 1 data written.\n", filename, n_written);
            return 1;
        }

        n_written = fwrite(&nchannels, sizeof(unsigned int), 1, f);
        if (n_written != 1){
            fprintf(stderr, "Error writing channels to file %s: %d of 1 data written.\n", filename, n_written);
            return 1;
        }
    }

    // The total data this rank will be writing
    n_data = nr_local*nc_local*nchannels;

    // This is a little inefficient, but for simplicity we will create a local,
    // unpadded arrays so we can do a single communication.  This is safe because
    // by specification, Rank 0 has the largest possible array.
    float* write_data = (float*) malloc(n_data*sizeof(float));
    if(write_data == NULL){
        fprintf(stderr, "Error allocating temporary array data.\n");
        return 1;
    }
    memset(write_data, 0, n_data*sizeof(float));

    // Loop over the local, non-padded part of the data and fill
    // with the data
    for(int i=0;i<nr_local;i++){

        // Copy the ith row into the ith row of write_data.
        // The ith row starts padding_nr rows into the provided array
        // and the data we care about starts padding_nc columns into
        // that row.
        // The length of the row is nc_local*nchannels.
        memcpy(write_data + i*nc_local*nchannels,
               arr + (padding_nr*nc_padded + i*nc_padded + padding_nc)*nchannels,
               nc_local*nchannels*sizeof(float));
    }

    if (rank == 0){

        // Rank 0 can directly write its own data
        n_written = fwrite(write_data, sizeof(float), n_data, f);
        if(n_written != n_data){
            fprintf(stderr, "Error writing file %s:expected %d bytes wrote %d bytes.\n", filename, n_data, n_written);
            return 1;
        }

        // Rank 0 receives chunks of data from all other processors and writes
        // it to disk
        int nr_p, nc_p;
        for(int p=1; p<size; p++){

            // Determine the remote ranks array properties (local rows and
            // columns, amount of data, etc.)
            nr_p = subarray_size(nr_global, size, p);
            nc_p = subarray_size(nc_global, 1, p);
            n_data = nr_p*nc_p*nchannels;

            memset(write_data, 0, n_data*sizeof(float));

            // Rank 0 obtains part of the array from every other rank
            MPI_Status status;
            MPI_Recv(write_data, n_data, MPI_FLOAT, p,  0,  comm, &status);

            n_written = fwrite(write_data, sizeof(float), n_data, f);
            if(n_written != n_data){
                fprintf(stderr, "Error writing file %s:expected %d bytes wrote %d bytes.\n", filename, n_data, n_written);
                return 1;
            }
        }

        fclose(f);
    }
    else{
        // All other ranks just send their respective chunk of the array
        // to rank 0
        MPI_Send(write_data, n_data, MPI_FLOAT, 0,  0,  comm);
    }

    free(write_data);

    return 0;

}


/*
* Writes an nr_global x nc_global x 4 RGBA array to disk, using MPI-style IO
*
* Arguments:
*   arr: The data array pointer to read to
*   nr_local: number of rows in the array for this rank, without padding
*   nc_local: number of columns in the array for this rank, without padding
*   nr_padded: number of rows in the array for this rank, with padding
*   nc_padded: number of columns in the array for this rank, with padding
*   nr_global: number of rows in the grid, total over all ranks
*   nc_global: number of columns in the grid, total over all ranks
*   padding_nr: number of padding rows in the array for this rank
*   padding_nc: number of padding columns in the array for this rank
*   comm: MPI communicator the grid is distributed over
*   filename: string name of the file to read
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int write_simple_array_format_dist_mpiio(float* arr,
                                         unsigned int nr_local, unsigned int nc_local,
                                         unsigned int nr_padded, unsigned int nc_padded,
                                         unsigned int nr_global, unsigned int nc_global,
                                         unsigned int padding_nr, unsigned int padding_nc,
                                         MPI_Comm comm,
                                         char* filename){

    // File handle
    FILE* f;

    // Total data to write, measured data written
    unsigned int n_data, n_written;
    unsigned int nchannels = 4;

    // header size in bytes; three unsigned ints
    int header_size = 12;

    int rank, size;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    // Only rank 0 should manage file creation and write the header
    if (rank == 0){
        // Open the file
        f = fopen(filename, "wb");
        if (f == NULL) {
            fprintf(stderr, "Error opening file %s for write.\n", filename);
            return 1;
        }

        // Write the header
        n_written = fwrite(&nr_global, sizeof(unsigned int), 1, f);
        if (n_written != 1){
            fprintf(stderr, "Error writing nr to file %s: %d of 1 data written.\n", filename, n_written);
            return 1;
        }

        n_written = fwrite(&nc_global, sizeof(unsigned int), 1, f);
        if (n_written != 1){
            fprintf(stderr, "Error writing nc to file %s: %d of 1 data written.\n", filename, n_written);
            return 1;
        }

        n_written = fwrite(&nchannels, sizeof(unsigned int), 1, f);
        if (n_written != 1){
            fprintf(stderr, "Error writing channels to file %s: %d of 1 data written.\n", filename, n_written);
            return 1;
        }
        fclose(f);
    }

    // The total data this rank will be writing
    n_data = nr_local*nc_local*nchannels;

    // This is a little inefficient, but for simplicity we will create a local,
    // unpadded arrays so we can do a single communication.  This is safe because
    // by specification, Rank 0 has the largest possible array.
    float* write_data = (float*) malloc(n_data*sizeof(float));
    if(write_data == NULL){
        fprintf(stderr, "Error allocating temporary array data.\n");
        return 1;
    }
    memset(write_data, 0, n_data*sizeof(float));

    // Loop over the local, non-padded part of the data and fill
    // with the data
    for(int i=0;i<nr_local;i++){

        // Copy the ith row into the ith row of write_data.
        // The ith row starts padding_nr rows into the provided array
        // and the data we care about starts padding_nc columns into
        // that row.
        // The length of the row is nc_local.
        memcpy(write_data + i*nc_local*nchannels,
               arr + (padding_nr*nc_padded + i*nc_padded + padding_nc)*nchannels,
               nc_local*nchannels*sizeof(float));
    }

    MPI_File out_file;
    MPI_Status status;
    MPI_Offset offset;

    // Perform the actual write using MPI I/O
    // <student>

    // </student>

    free(write_data);

    return 0;

}