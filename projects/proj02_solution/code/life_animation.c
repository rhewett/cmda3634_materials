/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "array_rgba.h"
#include "life.h"

int main(int argc, char** argv){

    if(argc != 5){
        printf("Incorrect number of parameters.  Correct usage:\n./life_animation nr nc p nt\n");
        return 1;
    }

    // Extract command line arguments.
    unsigned int nr = atoi(argv[1]);
    unsigned int nc = atoi(argv[2]);
    float p = atof(argv[3]);
    unsigned int nt = atoi(argv[4]);

    int error = 0;

    ArrayRGBA g_curr;
    ArrayRGBA g_next;

    // This was not required by the spec but it is an additional safety against using an
    // unallocated array.
    nullify_ArrayRGBA(&g_curr);
    nullify_ArrayRGBA(&g_next);

    // Allocate the required arrays.
    read_ArrayRGBA(&g_curr, "glider_train.arr");

    // Initialize the required arrays.
    error = allocate_ArrayRGBA(&g_next, g_curr.nr, g_curr.nc);
    if (error) return 3;
    initialize_ArrayRGBA(&g_next);

    // Perform the task.
    char outfile[50];

    // Output the solution at t = 0; same for both true and computed
    memset(outfile, 0, 50*sizeof(char));
    sprintf(outfile, "animation/generation_%05d.arr", 0);
    write_ArrayRGBA(&g_curr, outfile);

    // Evaluate the solution 1 step at a time and output each generation.
    for(int k=1; k<=nt; k++){
        life_simulation(&g_curr, &g_next, 1);

        memset(outfile, 0, 50*sizeof(char));
        sprintf(outfile, "animation/generation_%05d.arr", k);
        write_ArrayRGBA(&g_curr, outfile);
    }

    // Clean up the memory
    deallocate_ArrayRGBA(&g_curr);
    deallocate_ArrayRGBA(&g_next);

    return 0;
}