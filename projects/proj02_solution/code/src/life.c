/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "life.h"
#include "array_rgba.h"


/*
* Helper function to the linear index given array indices.
*
* Assumes 4 floats per i,j index.
*
* Arguments:
*   i: current row index
*   j: current col index
*   nc: number of cols
*
* Returns:
*   linear index
*/

int ij_to_idx(int i, int j, int nc) {
    return (i*nc + j)*4;
}


/*
* Helper function to compute the index to the right of i, with periodic
* boundaries.
*
* Arguments:
*   i: current index
*   n: number of entries
*
* Returns:
*   index to the right of i, assuming periodic boundaries
*/

int right_shift(int i, int n){
    return (i+1+n)%n;
}


/*
* Helper function to compute the index to the left of i, with periodic
* boundaries.
*
* Arguments:
*   i: current index
*   n: number of entries
*
* Returns:
*   index to the left of i, assuming periodic boundaries
*/

int left_shift(int i, int n){
    return (i-1+n)%n;
}


/*
* Helper function for computing uniform 0,1 random samples.
*/

float rand_uniform(){
    return (float)rand() / (float)RAND_MAX;
}


/*
* Creates a random initialization, where colors are random and life/death is
* given by probability p.
*
* Arguments:
*   g: Pointer to the array structure that will store the output
*   p: Probability of life.
*
* Returns:
*   Error Code:
*      * 1: array is improperly allocated.
*/

int initialize_random_generation(ArrayRGBA* g, float p) {

    // Error checks
    if (g->data == NULL) {
        fprintf(stderr, "Error: g is null in initialize_random_generation.\n");
        return 1;
    }

    unsigned int nr = g->nr;
    unsigned int nc = g->nc;

    for(int i=0; i<nr; ++i) {

        for(int j=0; j<nc; ++j) {

            int k_ij = ij_to_idx(i, j, nc);

            float v = rand_uniform();

            // Alive
            if(v < p){
                // RGB uniform(0,1)
                g->data[k_ij + 0] = rand_uniform();
                g->data[k_ij + 1] = rand_uniform();
                g->data[k_ij + 2] = rand_uniform();
                g->data[k_ij + 3] = 1;
            }
            else{ // Dead
                g->data[k_ij + 0] = 0.0;
                g->data[k_ij + 1] = 0.0;
                g->data[k_ij + 2] = 0.0;
                g->data[k_ij + 3] = 0.0;
            }
        }
    }

    return 0;
}


/*
* Helper function for determining number of living neighbors.
*
* Arguments:
*   g: Pointer to the array structure to count from.
*   k_UL: up, left linear index
*   k_Uj: up, middle linear index
*   k_UR: up, right linear index
*   k_iL: middle, left linear index
*   k_iR: middle, right linear index
*   k_DL: down, left linear index
*   k_Dj: down, middle linear index
*   k_DR: down, right linear index
*
* Returns:
*   Number of living neighbors.
*/

float living_neighbors(ArrayRGBA* g,
                       int k_UL, int k_Uj, int k_UR,
                       int k_iL,           int k_iR,
                       int k_DL, int k_Dj, int k_DR){
    return g->data[k_UL+3] + g->data[k_Uj+3] + g->data[k_UR+3] +
           g->data[k_iL+3]                   + g->data[k_iR+3] +
           g->data[k_DL+3] + g->data[k_Dj+3] + g->data[k_DR+3];
}


/*
* Helper function for determining average color of living neighbors.
*
* Assumes dead neighbors are color 0.0.
*
* Arguments:
*   g: Pointer to the array structure to count from.
*   C: Color index R=0, G=1, B=2.
*   living_neighbors: number of living neighbors
*   k_UL: up, left linear index
*   k_Uj: up, middle linear index
*   k_UR: up, right linear index
*   k_iL: middle, left linear index
*   k_iR: middle, right linear index
*   k_DL: down, left linear index
*   k_Dj: down, middle linear index
*   k_DR: down, right linear index
*
* Returns:
*   Number of living neighbors.
*/

float average_color(ArrayRGBA* g, int C, int living_neighbors,
                    int k_UL, int k_Uj, int k_UR,
                    int k_iL,           int k_iR,
                    int k_DL, int k_Dj, int k_DR){
    return (g->data[k_UL+C] + g->data[k_Uj+C] + g->data[k_UR+C] +
            g->data[k_iL+C]                   + g->data[k_iR+C] +
            g->data[k_DL+C] + g->data[k_Dj+C] + g->data[k_DR+C]) / living_neighbors;
}


/*
* Evaluates a single timestep of life simulater.
*
* Arguments:
*   g_curr: Pointer to the array structure containing current state
*   g_next: Pointer to the array structure containing next state, contains the output
*
* Returns:
*   Error Code:
*      * 1: an array is improperly allocated.
*      * 2: dimension mismatch in arrays.
*/

int life_evolution(ArrayRGBA* g_curr, ArrayRGBA* g_next) {

    // Error checks
    if ((g_curr->data == NULL) || (g_next->data == NULL)) {
        fprintf(stderr, "Error: g_curr (%p) or g_next (%p) are null in life_timestep.\n",
                g_curr->data, g_next->data);
        return 1;
    }
    if ((g_curr->nc != g_next->nc) ||
        (g_curr->nr != g_next->nr)) {
        fprintf(stderr, "Error: g_curr (%u, %u) or g_next (%u, %u) dimension mismatch.\n",
                g_curr->nc, g_curr->nr, g_next->nc, g_next->nr);
        return 2;
    }

    // Convenience variables for shortening code
    unsigned int nr = g_curr->nr;
    unsigned int nc = g_curr->nc;

    // Loop over both spatial dimensions
    for(int i=0; i<nr; ++i) {
        int i_U = left_shift(i, nr);
        int i_D = right_shift(i, nr);

        for(int j=0; j<nc; ++j) {
            int j_L = left_shift(j, nc);
            int j_R = right_shift(j, nc);

            int k_UL = ij_to_idx(i_U, j_L, nc);
            int k_Uj = ij_to_idx(i_U, j,   nc);
            int k_UR = ij_to_idx(i_U, j_R, nc);

            int k_iL = ij_to_idx(i,   j_L, nc);
            int k_ij = ij_to_idx(i,   j,   nc);
            int k_iR = ij_to_idx(i,   j_R, nc);

            int k_DL = ij_to_idx(i_D, j_L, nc);
            int k_Dj = ij_to_idx(i_D, j,   nc);
            int k_DR = ij_to_idx(i_D, j_R, nc);

            int L_ij = living_neighbors(g_curr,
                                        k_UL, k_Uj, k_UR,
                                        k_iL,       k_iR,
                                        k_DL, k_Dj, k_DR);

            int ij_status = (int)g_curr->data[k_ij+3];

            if((ij_status == 0) && (L_ij == 3)){
                // Red
                g_next->data[k_ij+0] = average_color(g_curr, 0, L_ij,
                                                     k_UL, k_Uj, k_UR,
                                                     k_iL,       k_iR,
                                                     k_DL, k_Dj, k_DR);
                // Green
                g_next->data[k_ij+1] = average_color(g_curr, 1, L_ij,
                                                     k_UL, k_Uj, k_UR,
                                                     k_iL,       k_iR,
                                                     k_DL, k_Dj, k_DR);
                // Blue
                g_next->data[k_ij+2] = average_color(g_curr, 2, L_ij,
                                                     k_UL, k_Uj, k_UR,
                                                     k_iL,       k_iR,
                                                     k_DL, k_Dj, k_DR);
                // Alive
                g_next->data[k_ij+3] = 1.0;
            }
            else if((ij_status == 1) &&
                    ((L_ij == 2) || (L_ij == 3))){
                g_next->data[k_ij+0] = g_curr->data[k_ij+0];
                g_next->data[k_ij+1] = g_curr->data[k_ij+1];
                g_next->data[k_ij+2] = g_curr->data[k_ij+2];
                g_next->data[k_ij+3] = g_curr->data[k_ij+3];
            }
            else{
                g_next->data[k_ij+0] = 0.0;
                g_next->data[k_ij+1] = 0.0;
                g_next->data[k_ij+2] = 0.0;
                g_next->data[k_ij+3] = 0.0;
            }
        }
    }

    return 0;
}


/*
* Evaluates multiple timesteps of a life simulator.
*
* On output, g_curr contains the solution after nt timesteps.
*
* Arguments:
*   g_curr: Pointer to the array structure containing current state
*   g_next: Pointer to the array structure containing next state
*   nt: number of timesteps to evaluate
*
* Returns:
*   Error Code:
*      * 1: an array is improperly allocated.
*      * 2: dimension mismatch in arrays.
*/

int life_simulation(ArrayRGBA* g_curr, ArrayRGBA* g_next, int nt) {

    // Error checks
    if ((g_curr->data == NULL) || (g_next->data == NULL)) {
        fprintf(stderr, "Error: g_curr (%p), or g_next (%p) are null in life_timestep.\n",
                 g_curr->data, g_next->data);
        return 1;
    }
    if ((g_curr->nc != g_next->nc) ||
        (g_curr->nr != g_next->nr)) {
        fprintf(stderr, "Error: g_curr (%u, %u), or g_next (%u, %u) dimension mismatch.\n",
                g_curr->nc, g_curr->nr, g_next->nc, g_next->nr);
        return 2;
    }


    // Perform nt timesteps of the simulation and swap the outputs so that g_curr is ready for the next step.
    for(int k=0; k<nt; ++k) {

        life_evolution(g_curr, g_next);

        // swap the pointers around, no reallocation
        // This is a space to be very careful.  This function takes pointers to our _types_ as arguments.
        // The pointers themselves are passed-by-copy.  Therefore, we cannot swap these pointers.
        // However, we can swap their contents!  (We've already tested that the dimensions match.)
        float* temp;
        temp = g_curr->data;
        g_curr->data = g_next->data;
        g_next->data = temp;
    }

    return 0;
}
