/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "array_rgba.h"
#include "life.h"

int main(int argc, char** argv){

    if(argc != 5){
        printf("Incorrect number of parameters.  Correct usage:\n./life_timing nr nc p nt\n");
        return 1;
    }

    // Setup the timers.
    clock_t start, stop;
    double simulation_time = 0.0;

    // Extract command line arguments.
    unsigned int nr = atoi(argv[1]);
    unsigned int nc = atoi(argv[2]);
    float p = atof(argv[3]);
    unsigned int nt = atoi(argv[4]);
    srand(time(NULL));

    int error = 0;

    ArrayRGBA g_curr;
    ArrayRGBA g_next;

    // This was not required by the spec but it is an additional safety against using an
    // unallocated array.
    nullify_ArrayRGBA(&g_curr);
    nullify_ArrayRGBA(&g_next);

    // Allocate the required arrays.
    error = allocate_ArrayRGBA(&g_curr, nr, nc);
    if (error) return 2;
    error = allocate_ArrayRGBA(&g_next, nr, nc);
    if (error) return 3;

    // Initialize the required arrays.
    initialize_ArrayRGBA(&g_curr);
    initialize_ArrayRGBA(&g_next);

    // Perform the task.
    start = clock();

    // Evaluate the random initial
    initialize_random_generation(&g_curr, p);

    // Perform nt steps of the simulation
    life_simulation(&g_curr, &g_next, nt);

    stop = clock();
    simulation_time = (double)(stop - start) / CLOCKS_PER_SEC;

    // Clean up the memory
    deallocate_ArrayRGBA(&g_curr);
    deallocate_ArrayRGBA(&g_next);

    printf("%d %d %d %f\n", nr, nc, nt, simulation_time);

    return 0;
}