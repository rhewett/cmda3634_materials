Project 02 (Solution by Russell J. Hewett)
==========================================


Here is a solution to Project 02.  It does not complete all of the experiments
(i.e., scripts are not provided) but it is a complete implementation of the
required coding steps.


Layout
------

* The makefile and main programs are in the root directory.
* The `include` directory contains all user-written header files.
* The `src` directory contains all user-written source files.


Building
--------

To compile all programs, run

    make

To build a specific program, run (for example)

   make life_timing

Executing
---------

Each program takes a few command line arguments to specify the parameters of
the Life simulation.  These are documented with the programs.

To run `life_timing` on a 100 x 100 input, with p=0.2, for 100 steps:

    ./life_timing 100 100 0.2 100

To run `life_images` on a 100 x 100 input, with p=0.2, for 100 steps and output the 1, 3, and 5 steps:

    ./life_images 0 100 100 0.2 100 1 3 5

To run `life_images` on the random image for 100 steps and output the 1, 3, and 5 steps:

    ./life_images 1 initial_states/random_10.arr 100 1 3 5

Note, both of those will stop at either the maximum nt or the maximum needed generation, whichever comes first.

If you run the `life_images` program for the required timesteps, you can modify and run the `plot_images.py` script to view the required plots.

