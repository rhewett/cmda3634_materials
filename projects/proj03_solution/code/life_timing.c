/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <omp.h>

#include "omp_helpers.h"
#include "array_rgba.h"
#include "life.h"

int main(int argc, char** argv){

    // Setup OpenMP and the parallel random number generation.
    int n_threads;

    // Get the max number of threads (if we don't set it from the CLI).
    // Note, this is safe if we decide to take the number of threads from
    // the CLI.
#pragma omp parallel default(none) \
                     shared(n_threads)
    {
        n_threads = omp_get_num_threads();
    }

    if(argc != 5){
        printf("Incorrect number of parameters.  Correct usage:\n./life_timing nr nc p nt\n");
        return 1;
    }

    // Setup the timers.
    double start, stop;
    double simulation_time = 0.0;

    // Extract command line arguments.
    unsigned int nr = atoi(argv[1]);
    unsigned int nc = atoi(argv[2]);
    float p = atof(argv[3]);
    unsigned int nt = atoi(argv[4]);

    struct drand48_data random_buffers[n_threads];

    int seed_base = (int) omp_get_wtime();
    seed_buffers_omp(seed_base, random_buffers);

    int error = 0;

    ArrayRGBA g_curr;
    ArrayRGBA g_next;

    // This was not required by the spec but it is an additional safety against using an
    // unallocated array.
    nullify_ArrayRGBA(&g_curr);
    nullify_ArrayRGBA(&g_next);

    // Allocate the required arrays.
    error = allocate_ArrayRGBA(&g_curr, nr, nc);
    if (error) return 2;
    error = allocate_ArrayRGBA(&g_next, nr, nc);
    if (error) return 3;

    // Initialize the required arrays.
    initialize_ArrayRGBA(&g_curr);
    initialize_ArrayRGBA(&g_next);

    // Perform the task.
    start = omp_get_wtime();

    // Evaluate the random initial
    initialize_random_generation(&g_curr, p, random_buffers);

    // Perform nt steps of the simulation
    life_simulation(&g_curr, &g_next, nt);

    stop = omp_get_wtime();
    simulation_time = (stop - start);

    // Clean up the memory
    deallocate_ArrayRGBA(&g_curr);
    deallocate_ArrayRGBA(&g_next);

    printf("%d %d %d %f\n", nr, nc, nt, simulation_time);

    return 0;
}