
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from simple_array_io import save_array, load_array

# c = []
# c.append(load_array("animation/generation_00000.arr"))
# c.append(load_array("animation/generation_00001.arr"))
# c.append(load_array("animation/generation_00002.arr"))
# c.append(load_array("animation/generation_00003.arr"))
# c.append(load_array("animation/generation_00004.arr"))
# c.append(load_array("animation/generation_00005.arr"))


# for i in range(len(c)):

#     plt.figure()
#     plt.imshow(c[i])

G = []
for i in range(301):
    G.append(load_array(f"animation/generation_{i:05}.arr"))


def animate_evolution(G):
    """
    Creates a 2D animation of a wave simulation.

    Automatically selects color-scale.  In the event of instability or
    a wild range of values, some components might not be easily seen.
    Consider tweaking this scale if you need to.

    Parameters
    ----------
    G: list-like
       A list of 2D generations arrays to animate.

    Returns
    -------
    Matplotlib animation class instance.

    """

    fig = plt.figure()

    cmin, cmax = 0, 1

    # Add extent to adjust the coordinates
    # Transpose the data so the correct axis maps to x and y
    im = plt.imshow(G[0], clim=(cmin,cmax), cmap='gray', extent=[0,1,1,0])

    # Need dt to plot the times
    sh = G[0].shape

    # Set the title
    # plt.title(rf"gen$={0:03}$")

    def animate(i):
        im.set_data(G[i])
        # plt.title(rf"gen$={i:03}$")
        return (im,)

    ani = animation.FuncAnimation(fig, animate, interval=50, repeat_delay=1000, frames=len(G))

    return ani


ani = animate_evolution(G)

plt.show()


