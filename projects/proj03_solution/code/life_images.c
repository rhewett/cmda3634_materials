/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <omp.h>

#include "omp_helpers.h"

#include "array_rgba.h"
#include "life.h"

int main(int argc, char** argv){

    // Setup OpenMP and the parallel random number generation.
    int n_threads;

    // Get the max number of threads (if we don't set it from the CLI).
    // Note, this is safe if we decide to take the number of threads from
    // the CLI.
#pragma omp parallel default(none) \
                     shared(n_threads)
    {
        n_threads = omp_get_num_threads();
    }

    if(argc < 2){
        printf("Incorrect number of parameters.  Correct usage:\n"
               "    ./life_images 0 nr nc p nt t_plot0 t_plot1 ...\n"
               " -or-\n"
               "    ./life_images 1 <filename> nt t_plot0 t_plot1 ...\n"
               );
        return 1;
    }

    int input_style = atoi(argv[1]);
    int input_index_start = -1;

    unsigned int nr, nc, nt;
    float p;
    char* filename_in;
    struct drand48_data random_buffers[n_threads];

    // Extract command line arguments.
    if (input_style == 0){

        if(argc < 6){
            printf("Incorrect number of parameters.  Correct usage:\n"
                   "    ./life_images 0 nr nc p nt t_plot0 t_plot1 ...\n"
                   );
            return 1;
        }

        nr = atoi(argv[2]);
        nc = atoi(argv[3]);
        p = atof(argv[4]);
        nt = atoi(argv[5]);

        input_index_start = 6;

        int seed_base = (int) omp_get_wtime();
        seed_buffers_omp(seed_base, random_buffers);
    }
    else if (input_style == 1){
        if(argc < 4){
            printf("Incorrect number of parameters.  Correct usage:\n"
                   "    ./life_images 1 <filename> nt t_plot0 t_plot1 ...\n"
                   );
            return 1;
        }
        filename_in = argv[2];
        nt = atoi(argv[3]);

        input_index_start = 4;
    }
    else{
        printf("Incorrect input structure.  Correct usage:\n"
               "    ./life_images 0 nr nc p nt t_plot0 t_plot1 ...\n"
               " -or-\n"
               "    ./life_images 1 <filename> nt t_plot0 t_plot1 ...\n"
               );
        return 1;
    }

    // // Extract command line arguments.
    // unsigned int nr = atoi(argv[1]);
    // unsigned int nc = atoi(argv[2]);
    // float p = atof(argv[3]);
    // unsigned int nt = atoi(argv[4]);

    int error = 0;

    ArrayRGBA g_curr;
    ArrayRGBA g_next;

    // This was not required by the spec but it is an additional safety against using an
    // unallocated array.
    nullify_ArrayRGBA(&g_curr);
    nullify_ArrayRGBA(&g_next);

    if(input_style == 0){
        // Setup for random input
        // Allocate the required arrays.
        error = allocate_ArrayRGBA(&g_curr, nr, nc);
        if (error) return 2;
        error = allocate_ArrayRGBA(&g_next, nr, nc);
        if (error) return 3;

        // Initialize the required arrays.
        initialize_ArrayRGBA(&g_curr);
        initialize_ArrayRGBA(&g_next);

        // Evaluate the initial conditions at t=-dt and t=0
        initialize_random_generation(&g_curr, p, random_buffers);
    }
    else{
        // Setup from file
        // Allocate the required arrays.
        read_ArrayRGBA(&g_curr, filename_in);

        // Initialize the required arrays.
        error = allocate_ArrayRGBA(&g_next, g_curr.nr, g_curr.nc);
        if (error) return 3;
        initialize_ArrayRGBA(&g_next);
    }

    // Perform the task.
    char outfile[50];

    // Loop over the remaining times and run the simulation to that time.
    // We have to reset every time because we don't enforce that the inputs are
    // sorted.  If we had them sorted, we could perform the simulation incrementally.
    int t = 0;
    unsigned int hash;
    for(int k=input_index_start; k<argc; k++){

        int t_plot = atoi(argv[k]);
        printf("%d\n", t_plot);


        // Perform nt steps of the simulation
        while((t < t_plot) && (t < nt)){
            // Perform 1 steps of the simulation
            hash_ArrayRGBA(&g_curr, &hash);
            printf("Hash(g_%d) = %d\n", t, hash);

            life_simulation(&g_curr, &g_next, 1);
            t += 1;
        }

        // Save the simulated solutution
        memset(outfile, 0, 50*sizeof(char));
        sprintf(outfile, "images/computed_%05d.arr", t);
        write_ArrayRGBA(&g_curr, outfile);

        printf("Saving images from t=%d.\n", t_plot);
    }
    hash_ArrayRGBA(&g_curr, &hash);
    printf("Hash(g_%d) = %d\n", t, hash);

    // Clean up the memory
    deallocate_ArrayRGBA(&g_curr);
    deallocate_ArrayRGBA(&g_next);

    return 0;
}