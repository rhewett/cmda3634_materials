/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

#include "array_rgba.h"
#include "simple_array_io.h"



/*
* Allocate an ArrayRGBA struct.
*
* Arguments:
*   arr: The nr x nc x 4 array struct to allocate into
*   m: number of rows in the array
*   n: number of columns in the array
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int allocate_ArrayRGBA(ArrayRGBA* arr, unsigned int nr, unsigned int nc) {

    arr->nr = nr;
    arr->nc = nc;

    // rows x columns x 4
    unsigned int n_data = nr*nc*4;

    arr->data = malloc(n_data*sizeof(float));

    if (arr->data == NULL){
        fprintf(stderr, "Error allocating 2D int array.\n");
        return 1;
    }

    return 0;
}


/*
* Deallocate an ArrayRGBA struct.
*
* Arguments:
*   arr: The array struct to deallocate
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int deallocate_ArrayRGBA(ArrayRGBA* arr){

    arr->nr = 0;
    arr->nc = 0;

    free(arr->data);

    arr->data = NULL;

    return 0;
}


/*
* Initialize an ArrayRGBA struct to 0.
*
* Arguments:
*   arr: The array struct to initialize
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int initialize_ArrayRGBA(ArrayRGBA* arr){

    unsigned int n_data = arr->nr * arr->nc * 4;

    memset(arr->data, 0, n_data*sizeof(float));

    return 0;
}


/*
* Nullify an ArrayRGBA struct.
*
* Nullification means pointers are set to NULL and dimensions are set to 0.
*
* Arguments:
*   arr: The array struct to deallocate
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int nullify_ArrayRGBA(ArrayRGBA* arr){

    arr->nr = 0;
    arr->nc = 0;
    arr->data = NULL;

    return 0;
}


/*
* Copy an ArrayRGBA struct to another ArrayRGBA struct.
*
* Note: Assumes destination array is already allocated.
*
* Arguments:
*   src: The array to copy from
*   dest: The array to copy to
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int copy_ArrayRGBA(ArrayRGBA* src, ArrayRGBA* dest){

    if ((src->nr != dest->nr) ||
        (src->nc != dest->nc)){
        fprintf(stderr, "Size mismatch in int array copy (src=%dX%d dest=%d,%d).\n",
                src->nr, src->nc, dest->nr, dest->nc);
        return 1;
    }

    if (src->data == NULL){
        fprintf(stderr, "Source array NULL in int array copy.\n");
        return 2;
    }

    if (dest->data == NULL){
        fprintf(stderr, "Destination array NULL in int array copy.\n");
        return 3;
    }

    unsigned int n_data = src->nr * src->nc * 4;

    memcpy(dest->data, src->data, n_data*sizeof(float));

    return 0;
}


/*
* Write an ArrayRGBA struct to disk.
*
* Arguments:
*   arr: The array to write
*   filename: The filename to write to
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int write_ArrayRGBA(ArrayRGBA* arr, char* filename){

    FILE* f;

    // Open the file
    f = fopen(filename, "wb");
    if (f == NULL) {
        fprintf(stderr, "Error opening file %s for write.\n", filename);
        return 1;
    }

    unsigned int n_written;

    // Write the header
    n_written = fwrite(&arr->nr, sizeof(unsigned int), 1, f);
    if (n_written != 1){
        fprintf(stderr, "Error writing nr to file %s: %d of 1 data written.\n", filename, n_written);
        return 1;
    }

    n_written = fwrite(&arr->nc, sizeof(unsigned int), 1, f);
    if (n_written != 1){
        fprintf(stderr, "Error writing nc to file %s: %d of 1 data written.\n", filename, n_written);
        return 1;
    }

    // Cannot just write an int, need a ptr to it.
    unsigned int channels = 4;
    n_written = fwrite(&channels, sizeof(unsigned int), 1, f);
    if (n_written != 1){
        fprintf(stderr, "Error writing 4 to file %s: %d of 1 data written.\n", filename, n_written);
        return 1;
    }

    // Write the data
    printf("%d, %d, %d \n", arr->nr, arr->nc, channels);

    unsigned int n_data = arr->nc * arr->nr * 4;
    n_written = fwrite(arr->data, sizeof(float), n_data, f);
    if (n_written != n_data){
        fprintf(stderr, "Error writing data to file %s: %d of %d data written.\n", filename, n_written, n_data);
        return 1;
    }

    fclose(f);

    return 0;
}


/*
* Read an ArrayRGBA struct from disk.
*
* Arguments:
*   arr: The array to write
*   filename: The filename to write to
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int read_ArrayRGBA(ArrayRGBA* arr, char* filename){

    unsigned int n_channels, n_r, n_c;
    int err;
    err = read_simple_array_format(filename, &(arr->data), &n_r, &n_c, &n_channels);
    arr->nr = n_r;
    arr->nc = n_c;

    if (err){
        fprintf(stderr, "Error in the provided file.");
        return 1;
    }

    return 0;
}


/*
* Compute the hash of an RGBA array structure.
*
* Arguments:
*   arr: The array to write
*   hash: unsigned integer containing the output hash
*
* Returns:
*   error code, 0 for success, 1 for failure
*/

int hash_ArrayRGBA(ArrayRGBA* arr, unsigned int* hash){

    if (arr->data == NULL){
        fprintf(stderr, "Array NULL in int array copy.\n");
        return 1;
    }

    unsigned int nr = arr->nr;
    unsigned int nc = arr->nc;
    float* data = arr->data;

    unsigned int local_hash = 0;

#pragma omp parallel for default(none) \
                         shared(data,nr,nc) \
                         reduction(+:local_hash)
    for(unsigned int i=0; i<nr; ++i) {
        for(unsigned int j=0; j<nc; ++j) {
            unsigned int k_ij = 4*(i*nc + j);
            local_hash += i + j*data[k_ij+3]*(     (int)(data[k_ij+0]*255) +
                                                10*(int)(data[k_ij+1]*255) +
                                               100*(int)(data[k_ij+2]*255));
        }
    }

    *hash = local_hash;
    return 0;
}
