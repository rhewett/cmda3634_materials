#include <stdlib.h>
#include <omp.h>

#include "omp_helpers.h"


void seed_buffers_omp(int seed_base, struct drand48_data* random_buffers){

#pragma omp parallel default(none) \
                     shared(seed_base,random_buffers)
    {
        int thread = omp_get_thread_num();

        srand48_r(seed_base+thread, &random_buffers[thread]);
    }
}