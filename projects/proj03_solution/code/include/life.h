/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#ifndef __LIFE_H__
#define __LIFE_H__

#include <stdlib.h>

#include "array_rgba.h"

int initialize_random_generation(ArrayRGBA* g, float p, struct drand48_data* random_buffers);

int life_evolution(ArrayRGBA* u_curr, ArrayRGBA* u_next);

int life_simulation(ArrayRGBA* g_curr, ArrayRGBA* g_next, int nt);

#endif