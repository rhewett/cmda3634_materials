/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#ifndef __ARRAY_RGBA_H__
#define __ARRAY_RGBA_H__

typedef struct ArrayRGBA_tag {

    float* data;

    // nr X nc is number of rows X number of columns
    unsigned int nr;
    unsigned int nc;

    // There are always going to be 4 channels, so we don't store that

} ArrayRGBA;

int allocate_ArrayRGBA(ArrayRGBA* arr, unsigned int nr, unsigned int nc);
int deallocate_ArrayRGBA(ArrayRGBA* arr);
int nullify_ArrayRGBA(ArrayRGBA* arr);
int initialize_ArrayRGBA(ArrayRGBA* arr);
int copy_ArrayRGBA(ArrayRGBA* src, ArrayRGBA* dest);
int write_ArrayRGBA(ArrayRGBA* arr, char* filename);
int read_ArrayRGBA(ArrayRGBA* arr, char* filename);
int hash_ArrayRGBA(ArrayRGBA* arr, unsigned int* hash);


#endif