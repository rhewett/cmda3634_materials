/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#ifndef __SIMPLE_ARRAY_IO_H__
#define __SIMPLE_ARRAY_IO_H__

int read_simple_array_format(char* filename, float** data, unsigned int* n_r, unsigned int* n_c, unsigned int* n_channels);

#endif