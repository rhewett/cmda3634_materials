/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#ifndef __OMP_HELPERS_H__
#define __OMP_HELPERS_H__

void seed_buffers_omp(int seed_base, struct drand48_data* random_buffers);

#endif