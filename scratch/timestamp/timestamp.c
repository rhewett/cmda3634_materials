#include <stdio.h>

typedef struct Timestamp_tag {
    int seconds_since_epoch;
} Timestamp;

void print_Timestamp(Timestamp t){
    int days, hours, minutes, seconds;
    int s;

    s = t.seconds_since_epoch;
    days = s / (24*60*60);
    s -= days*(24*60*60);
    hours = s / (60*60);
    s -= hours*(60*60);
    minutes = s / (60);
    s -= minutes*(60);
    seconds = s;

    printf("days: %d\n", days);
    printf("hours: %d\n", hours);
    printf("minutes: %d\n", minutes);
    printf("seconds: %d\n", seconds);
}

Timestamp subtract_Timestamp(Timestamp t1, Timestamp t2){

    Timestamp t3;

    t3.seconds_since_epoch = t1.seconds_since_epoch - t2.seconds_since_epoch;

    return t3;
}

int main(){

    Timestamp t1;
    Timestamp t2;

    t1.seconds_since_epoch = 100000;
    t2.seconds_since_epoch = 10097;

    print_Timestamp(t1);
    print_Timestamp(t2);
    print_Timestamp(subtract_Timestamp(t1,t2));
}
