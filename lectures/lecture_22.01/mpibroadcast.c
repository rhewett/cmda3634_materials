#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);

    int tag = 0;
    int rank, n_procs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_procs);
    MPI_Status status;

    int root_rank = 0;

    int data;
    if (rank == root_rank) data = 4367;

    printf("Before Bcast Rank %d: value = %d.\n", rank, data);

    MPI_Bcast(&data, 1, MPI_INT, root_rank, MPI_COMM_WORLD);

    printf("After Bcast Rank %d: value = %d.\n", rank, data);
    MPI_Finalize();
}