#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);

    int tag = 0;
    int rank, n_procs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_procs);
    MPI_Status status;

    int data; // compute interesting data

    if (rank == 0) data = 4367;
    printf("Before Bcast Rank %d: value = %d.\n", rank, data);

    int level, partner, stride, recv_data;
    level = 0;
    stride = 1;

    while (stride < n_procs){
        if(rank < 2*stride){ // bottom half sends
            if (rank < stride){
                partner = rank + stride;
                if (partner < n_procs){
                    printf("Level %d (%d): %d sends to %d\n", level, stride, rank, partner);
                    MPI_Send(&data, 1, MPI_INT, partner, 0,  MPI_COMM_WORLD);
                }
            }
            else{
                partner = rank - stride;
                printf("Level %d (%d): %d recv from %d\n", level, stride, rank, partner);
                MPI_Recv(&data, 1, MPI_INT, partner, 0,  MPI_COMM_WORLD, &status);
            }
        }
        stride *= 2;
        level += 1;
    }
    printf("After Bcast Rank %d: value = %d.\n", rank, data);
    MPI_Finalize();
}