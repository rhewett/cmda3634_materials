#include <mpi.h>

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank != 0)
        MPI_Barrier(MPI_COMM_WORLD);

    MPI_Finalize();
}