#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);

    int rank;

    double tt = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    tt = MPI_Wtime() - tt;

    printf("Rank %d time to get rank is %lf.\n", rank, tt);

    MPI_Finalize();
}
