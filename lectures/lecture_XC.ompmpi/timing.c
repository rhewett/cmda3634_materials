#include <stdlib.h>
#include <stdio.h>

#include <time.h>
#include <math.h>

#include <mpi.h>
#include <omp.h>

#include "vector.h"

int main(int argc, char *argv[]){

    if(argc < 2){
        printf("WARNING: Missing arguments. \n");
        printf("Usage: mpirun -np #PROCS axpy_vector_test N  \n");
        printf("    N: vectors are Nx1\n");
        return 1;
    }

    MPI_Init(&argc, &argv);

    int n_threads = omp_get_max_threads();

    int rank, n_tasks;
    MPI_Comm_size(MPI_COMM_WORLD, &n_tasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int N = atoi(argv[1]);
    Vector x_global, y_global;
    Vector x_local, y_local;

    // Every rank gets a variable but only rank 0 maintains
    // the global state
    if(rank == 0){
        allocate_Vector(&x_global, N, MPI_COMM_SELF);
        allocate_Vector(&y_global, N, MPI_COMM_SELF);

        rand_fill_Vector(&x_global);
        rand_fill_Vector(&y_global);
    }

    // Every rank has a local segment to work on
    allocate_Vector(&x_local, N, MPI_COMM_WORLD);
    allocate_Vector(&y_local, N, MPI_COMM_WORLD);

    // Scatter global vectors
    scatter_Vector(&x_global, &x_local);
    scatter_Vector(&y_global, &y_local);

    // Do the work
    int n_trials = 10;
    double time_manual;
    double tt = 0.0;

    for (int i=0; i<n_trials; i++){
        float ip;
        MPI_Barrier(MPI_COMM_WORLD);
        tt = MPI_Wtime();
        inner_product(&x_local, &y_local, &ip);
        MPI_Barrier(MPI_COMM_WORLD);
        time_manual += MPI_Wtime() - tt;
    }

    if (rank == 0){

        printf("%d, %d, %d, %.10f\n", N, n_tasks, n_threads, time_manual/n_trials);

    }

    // cleanup
    if(rank == 0){
        deallocate_Vector(&x_global);
        deallocate_Vector(&y_global);
    }

    deallocate_Vector(&x_local);
    deallocate_Vector(&y_local);

    MPI_Finalize();

    return 0;
}
