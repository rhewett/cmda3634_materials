mpirun -np 1 -x OMP_NUM_THREADS=8 ./bin/timing 100000000
mpirun -np 2 -x OMP_NUM_THREADS=4 ./bin/timing 100000000
mpirun -np 4 -x OMP_NUM_THREADS=2 ./bin/timing 100000000
mpirun -np 8 -x OMP_NUM_THREADS=1 ./bin/timing 100000000