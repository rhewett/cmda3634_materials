mpirun -np 1 -x OMP_NUM_THREADS=8 --map-by ppr:1:numa --bind-to numa ./bin/timing 100000000
mpirun -np 2 -x OMP_NUM_THREADS=4 --map-by ppr:1:l3cache --bind-to l3cache ./bin/timing 100000000
mpirun -np 4 -x OMP_NUM_THREADS=2 --map-by ppr:2:l3cache --bind-to l3cache ./bin/timing 100000000
mpirun -np 8 -x OMP_NUM_THREADS=1 ./bin/timing 100000000
