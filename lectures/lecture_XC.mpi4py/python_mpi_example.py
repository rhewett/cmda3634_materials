# Import MPI first.  Note, this calls MPI_Init() automatically.
from mpi4py import MPI

import numpy as np

# Communicators are objects!
comm = MPI.COMM_WORLD

# Many of the functions you are used to seeing are members of the communicator
size = comm.Get_size()
rank = comm.Get_rank()

for p in range(size):
    if (rank == p):
        print(f"Hello, world! (from rank {rank})")
    comm.Barrier()

# We can create data on one rank and share it

n_data = 10
if(rank == 0):
    global_data = np.arange(n_data*size, dtype=np.float64)
else:
    global_data = None

# Need to "allocate" the receive buffer
local_data = np.empty(n_data, dtype=np.float64)

# Note, we don't specify the lengths, data type, etc
# We do need to be sure that the sizes are evenly divisible
comm.Scatter(global_data, local_data, root=0)

for p in range(size):
    if (rank == p):
        print(f"Rank {rank} data: {local_data}")
    comm.Barrier()

# We can also reduce the results:
local_sum = local_data.sum()
global_sum = np.empty(1, dtype=np.float64)
comm.Reduce(local_sum, global_sum, MPI.SUM, root=0)

for p in range(size):
    if (rank == p):
        print(f"Rank {rank} local_sum: {local_sum}, global_sum: {global_sum}")
    comm.Barrier()

# We can also broadcast that back:
comm.Bcast(global_sum, root=0)

for p in range(size):
    if (rank == p):
        print(f"Rank {rank} global_sum: {global_sum}")
    comm.Barrier()


# We do not have to call MPI_Finalize() when we are done
