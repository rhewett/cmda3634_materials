#!/bin/bash

echo "strong scalability"
echo "=================="
echo "experiment, n_trials, N_vector, axpy, inner_product, n_trials, N_matrix, matvec, matvec_triangular"

N_vector=10000000
N_matrix=5000
NTRIALS=3

echo -n "default,  " && ./bin/experiment      $N_vector $N_matrix $NTRIALS
echo -n "O0,       "      && ./bin/experiment_0    $N_vector $N_matrix $NTRIALS
echo -n "O1,       "      && ./bin/experiment_1    $N_vector $N_matrix $NTRIALS
echo -n "O2,       "      && ./bin/experiment_2    $N_vector $N_matrix $NTRIALS
echo -n "O3,       "      && ./bin/experiment_3    $N_vector $N_matrix $NTRIALS
echo -n "Ofast,    "   && ./bin/experiment_fast $N_vector $N_matrix $NTRIALS
echo -n "Og,       "      && ./bin/experiment_g    $N_vector $N_matrix $NTRIALS
echo -n "Os,       "      && ./bin/experiment_s    $N_vector $N_matrix $NTRIALS
