#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include <omp.h>

#include "vector.h"
#include "matrix.h"

int experiment_vector(int N, int n_trials){

    printf("%4d, %4d", n_trials, N);

    Vector x, y;
    Vector b;

    allocate_Vector(&x, N);
    allocate_Vector(&y, N);
    allocate_Vector(&b, N);

    float retval;

    rand_fill_Vector(&x);
    rand_fill_Vector(&y);
    zero_fill_Vector(&b);

    // set up number of repeat runs, and declare timers
    double t_start, t_end;
    double t_total, t_avg;

    // time axpy
    t_total = 0.0;
    t_avg = 0.0;
    for(int i=0; i<n_trials; i++){
        t_start = omp_get_wtime();
        axpy(0.5, &x, &y, &b);
        t_end = omp_get_wtime();
        t_total += t_end - t_start;
    }
    t_avg = t_total / n_trials;

    printf(", %.10f", t_avg);

    // time inner_product
    t_total = 0.0;
    t_avg = 0.0;
    for(int i=0; i<n_trials; i++){
        t_start = omp_get_wtime();
        inner_product(&x, &y, &retval);
        t_end = omp_get_wtime();
        t_total += t_end - t_start;
    }
    t_avg = t_total / n_trials;

    printf(", %.10f", t_avg);


    // cleanup
    deallocate_Vector(&x);
    deallocate_Vector(&y);
    deallocate_Vector(&b);

    return 0;
}

int experiment_matrix(int N, int n_trials){

    printf("%4d, %4d", n_trials, N);

    Matrix A;
    Vector x;
    Vector b;

    allocate_Matrix(&A, N, N);
    allocate_Vector(&x, N);
    allocate_Vector(&b, N);

    rand_fill_Matrix(&A);
    rand_fill_Vector(&x);
    zero_fill_Vector(&b);

    // set up number of repeat runs, and declare timers
    double t_start, t_end;
    double t_total, t_avg;

    // time matvec
    t_total = 0.0;
    t_avg = 0.0;
    for(int i=0; i<n_trials; i++){
        t_start = omp_get_wtime();
        matvec(&A, &x, &b);
        t_end = omp_get_wtime();
        t_total += t_end - t_start;
    }
    t_avg = t_total / n_trials;

    printf(", %.10f", t_avg);

    // time matvec_triangular
    t_total = 0.0;
    t_avg = 0.0;
    for(int i=0; i<n_trials; i++){
        t_start = omp_get_wtime();
        matvec_triangular(&A, &x, &b);
        t_end = omp_get_wtime();
        t_total += t_end - t_start;
    }
    t_avg = t_total / n_trials;

    printf(", %.10f", t_avg);

    // cleanup
    deallocate_Matrix(&A);
    deallocate_Vector(&x);
    deallocate_Vector(&b);

    return 0;
}


int main(int argc, char *argv[]){

    if(argc < 4){
        printf("WARNING: Missing arguments. \n");
        return 1;
    }

    // read input arguments to get number of rows and columns
    int N_vector = atoi(argv[1]);
    int N_matrix = atoi(argv[2]);
    int n_trials = atoi(argv[3]);

    experiment_vector(N_vector, n_trials);
    experiment_matrix(N_matrix, n_trials);
    printf("\n");

    return 0;
}
