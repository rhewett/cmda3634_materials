#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

int randomize_array(float* data, int n){

    int n_data = n*n;

    for(int i=0; i<n_data; i++){
        data[i] = (float)rand() / (float)RAND_MAX;
    }

    return 0;
}

int compute_right(int i, int m){
    return (i+1+m)%m;
}

int ij_to_idx(int i, int j, int m, int n){
    return i*n + j;
}

int norm_of_windowed_average(float* data, int n, float* norm){

    *norm = 0.0;

    int idx, idx_right, idx_down, idx_down_right;
    int i_down, j_right;

    for(int i=0; i<n; i++){

        i_down = compute_right(i, n);

        for(int j=0; j<n; j++){

            j_right = compute_right(j, n);

            idx = ij_to_idx(i, j, n, n);
            idx_right = ij_to_idx(i, j_right, n, n);
            idx_down = ij_to_idx(i_down, j, n, n);
            idx_down_right = ij_to_idx(i_down, j_right, n, n);

            float local = 0.0;
            local += data[idx] + data[idx_right] + data[idx_down] + data[idx_down_right];
            local /= 4.0;

            *norm += local*local;
        }
    }

    *norm = sqrt(*norm);

    return 0;
}

int main(int argc, char** argv){

    // Set random seed.
    srand(0);

    // Extract command line arguments.
    int n = atoi(argv[1]);
    int n_trials = atoi(argv[2]);

    float* data = malloc(n*n*sizeof(float));
    memset(data, 0, n*n*sizeof(float));

    randomize_array(data, n);

    // Perform the task.
    float v;
    int i;

    clock_t start, stop;
    double total_time;

    for(i=0; i<n_trials; i++){
        start = clock();
        norm_of_windowed_average(data, n, &v);
        stop = clock();
        total_time += (double)(stop - start)/CLOCKS_PER_SEC;
    }

    printf("Pure C\n");
    printf("---------------------------------------------------------------\n");
    printf("Norm: %f\n", v);
    printf("Average time for n=%d: %f\n", n, total_time/n_trials);

    free(data);

    return 0;
}