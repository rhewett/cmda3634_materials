import numpy as np

# Plain Python code run through Cython
def norm_of_windowed_average_a(arr):

    n = arr.shape[0]

    norm = 0.0

    for i in range(n):
        down = (i+1)%n
        for j in range(n):
            right = (j+1)%n

            local = arr[i, j] + arr[i, right] + arr[down, j] + arr[down, right]
            local /= 4.0

            norm += local**2

    norm = np.sqrt(norm)

    return norm

# Cython using type hints only
cimport numpy as np
def norm_of_windowed_average_b(np.ndarray arr):

    cdef int n = arr.shape[0]

    cdef float local
    cdef float norm = 0.0

    cdef int i, j
    cdef int down, right

    for i in range(n):
        down = (i+1)%n
        for j in range(n):
            right = (j+1)%n

            local = arr[i, j] + arr[i, right] + arr[down, j] + arr[down, right]
            local /= 4.0

            norm += local**2

    norm = np.sqrt(norm)

    return norm

# Cython using type hints only + arrays type and dimension
def norm_of_windowed_average_c(float[:, :] arr):

    cdef int n = arr.shape[0]

    cdef float local
    cdef float norm = 0.0

    cdef int i, j
    cdef int down, right

    for i in range(n):
        down = (i+1)%n
        for j in range(n):
            right = (j+1)%n

            local = arr[i, j] + arr[i, right] + arr[down, j] + arr[down, right]
            local /= 4.0

            norm += local**2

    norm = np.sqrt(norm)

    return norm


# Cython using type hints only + contiguous arrays and other enhancements
import cython
@cython.boundscheck(False)
@cython.wraparound(False)
def norm_of_windowed_average_d(float[:, :] arr):

    cdef int n = arr.shape[0]

    cdef float local
    cdef float norm = 0.0

    cdef int i, j
    cdef int down, right

    for i in range(n):
        down = (i+1)%n
        for j in range(n):
            right = (j+1)%n

            local = arr[i, j] + arr[i, right] + arr[down, j] + arr[down, right]
            local /= 4.0

            norm += local**2

    norm = np.sqrt(norm)

    return norm
