import numpy as np

def norm_of_windowed_average(arr):

    n = arr.shape[0]

    norm = 0.0

    for i in range(n):
        down = (i+1)%n
        for j in range(n):
            right = (j+1)%n

            local = arr[i, j] + arr[i, right] + arr[down, j] + arr[down, right]
            local /= 4.0

            norm += local**2

    norm = np.sqrt(norm)

    return norm

def time_experiment(n, n_trials, func, title=None):
    import time

    np.random.seed(0)

    arr = np.random.rand(n,n).astype(np.float32)

    total_time = 0

    for i in range(n_trials):
        start = time.perf_counter()
        norm = func(arr)
        stop = time.perf_counter()
        total_time += stop - start

    print(f"{title}")
    print(f"Norm: {norm}")
    print(f"Average time for n={n}: {total_time/n_trials}")

if __name__ == '__main__':
    import sys

    n = int(sys.argv[1])
    n_trials = int(sys.argv[2])

    case = int(sys.argv[3])

    if case == 0 or case == 5:
        time_experiment(n, n_trials, norm_of_windowed_average, 
              "Python+NumPy")
        print("---------------------------------------------------------------")

    if case == 1 or case == 5:
        from array_manip_cython import norm_of_windowed_average_a
        time_experiment(n, n_trials, norm_of_windowed_average_a, 
              "Python+NumPy+Cython")
        print("---------------------------------------------------------------")

    if case == 2 or case == 5:
        from array_manip_cython import norm_of_windowed_average_b
        time_experiment(n, n_trials, norm_of_windowed_average_b, 
              "Python+NumPy+Cython(types)")
        print("---------------------------------------------------------------")

    if case == 3 or case == 5:
        from array_manip_cython import norm_of_windowed_average_c
        time_experiment(n, n_trials, norm_of_windowed_average_c, 
              "Python+NumPy+Cython(types, array structure)")
        print("---------------------------------------------------------------")

    if case == 4 or case == 5:
        from array_manip_cython import norm_of_windowed_average_d
        time_experiment(n, n_trials, norm_of_windowed_average_d, 
              "Python+NumPy+Cython(types, array structure, contiguity, unsafe)")
        print("---------------------------------------------------------------")
