#include <stdlib.h>
#include <stdio.h>

int main(int* argc, char** argv){

    int n_data = atoi(argv[1]);
    int* data = malloc(n_data*sizeof(int));
    FILE* f;

    for(int i=0; i<n_data; i++){
        data[i] = i*i;
    }

    f = fopen(argv[2], "wb");
    int n_written_data = fwrite(data, sizeof(int), n_data, f);

    printf("Wrote %d data!\n", n_written_data);

    free(data);
    fclose(f);
}
