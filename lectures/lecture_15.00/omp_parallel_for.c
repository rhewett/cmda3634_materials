#include <omp.h>
#include <stdio.h>

int main(int argc, char **argv) {
//    omp_set_num_threads(8);
    #pragma omp parallel for
    for (int i = 0; i < 32; i++) {
        int thread = omp_get_thread_num();
        int Nthreads = omp_get_num_threads();
        printf("thread %d of %d handling loop interation %d\n",thread,Nthreads,i);
    }
    return 0;
}
