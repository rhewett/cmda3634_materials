#include <omp.h>
#include <stdio.h>

void busy_work(int* arr, int n){
    for(int k=0;k<100;k++) for(int i=0; i<n; i++) arr[i] = n;
}

int main(int argc, char **argv) {

    omp_set_num_threads(8);

    int n = 16000000;
    int arr[n];

    busy_work(arr, n);

#pragma omp parallel
    {
        int thread = omp_get_thread_num();
        int Nthreads = omp_get_num_threads();
    
        busy_work(arr, n);
        printf("hello from thread %d of %d\n",thread,Nthreads);
    }

    busy_work(arr, n);

    return 0;
}

