#include <stdio.h>
#include <math.h>

typedef struct Vector_tag {
    float x;
    float y;
    float z;
} Vector;


float inner_product(Vector u, Vector v){

    return u.x*v.x + u.y*v.y + u.z*v.z;
}

float norm(Vector u){

    return sqrt(inner_product(u, u));
}


Vector axpy(float a, Vector u, Vector v){
    
    Vector w;

    w.x = a*u.x + v.x;
    w.y = a*u.y + v.y;
    w.z = a*u.z + v.z;

    return w;
}

int main(){

    // Two input 3-vectors
    Vector u;
    Vector v;

    // An output vector that we will re-use
    Vector w;

    // Initialize the input vectors
    u.x = 1.0;
    u.y = 1.0;
    u.z = 1.0;

    v.x = 1.1;
    v.y = 2.2;
    v.z = 3.3;

    printf("Initial setup:\n");
    printf("    ux, uy, uz = %f, %f, %f\n", u.x, u.y, u.z);
    printf("    vx, vy, vz = %f, %f, %f\n", v.x, v.y, v.z);
    printf("\n");


    // Compute the inner product of u and v
    // The inner product is the same as the dot product
    float ip_uv;

    ip_uv = inner_product(u, v);

    printf("Inner product:\n");
    printf("    <u, v> = %f\n", ip_uv);
    printf("\n");


    // Compute the norm of u
    float norm_u;

    norm_u = norm(u);

    printf("Norm:\n");
    printf("    ||u|| = sqrt{<u, u>} = %f\n", norm_u);
    printf("\n");

    // Compute the "axpy" operation (constant times a vector plus a vector.)
    // See http://www.netlib.org/lapack/explore-html/d8/daf/saxpy_8f.html
    // for more details.

    float alpha = 0.5;

    w = axpy(alpha, u, v);

    printf("AXPY: w = a*u + v\n");
    printf("    wx, wy, wz = %f, %f, %f\n", w.x, w.y, w.z);
    printf("\n");
 
    return 0;


}