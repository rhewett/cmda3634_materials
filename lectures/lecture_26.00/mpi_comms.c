#include <stdio.h>
#include <stdlib.h>

#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // printf("World rank %02d: world size is %d\n", world_rank, world_size);


    if(atoi(argv[1]) == 0){
        int self_size, self_rank;
        MPI_Comm_size(MPI_COMM_SELF, &self_size)&
        MPI_Comm_rank(MPI_COMM_SELF, &self_rank)&

        printf("World rank %02d: self size is %d and self rank is %d\n", 
               world_rank, self_size, self_rank);
    }

    if(atoi(argv[1]) == 1){
        MPI_Comm divided1;

        int color = world_rank % 2; // Divide the workers into even and odd
        int key = 0; // Can use this to change the rank order in the new communicator
        MPI_Comm_split(MPI_COMM_WORLD, color, key, &divided1);

        int divided1_size, divided1_rank;
        MPI_Comm_size(divided1, &divided1_size);
        MPI_Comm_rank(divided1, &divided1_rank);

        printf("World rank %02d: divided1 size is %d and divided1 rank is %d\n", 
               world_rank, divided1_size, divided1_rank);
    }

    if(atoi(argv[1]) == 2){
        MPI_Comm divided2;

        int color = world_rank % 3; 
        int key = 0; // Can use this to change the rank order in the new communicator
        MPI_Comm_split(MPI_COMM_WORLD, color, key, &divided2);

        int divided2_size, divided2_rank;
        MPI_Comm_size(divided2, &divided2_size);
        MPI_Comm_rank(divided2, &divided2_rank);

        printf("World rank %02d: divided2 size is %d and divided2 rank is %d\n", 
               world_rank, divided2_size, divided2_rank);
    }

    if(atoi(argv[1]) == 3){
        MPI_Comm divided3;

        int color = world_rank < 4;
        int key = 0; // Can use this to change the rank order in the new communicator
        MPI_Comm_split(MPI_COMM_WORLD, color, key, &divided3);

        int divided3_size, divided3_rank;
        MPI_Comm_size(divided3, &divided3_size);
        MPI_Comm_rank(divided3, &divided3_rank);

        printf("World rank %02d: divided3 size is %d and divided3 rank is %d\n", 
               world_rank, divided3_size, divided3_rank);
    }

    if(atoi(argv[1]) == 4){
        MPI_Comm cart34;

        int ndims = 2;
        int reorder = 0; // can choose to re-order the ranks
        
        int dims[2] = {3, 4};  // for 2D, rows X columns
        int periods[2] = {0, 0};  // is the decomposition periodic
        
        MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, reorder, &cart34);

        int cart34_size, cart34_rank;
        MPI_Comm_size(cart34, &cart34_size);
        MPI_Comm_rank(cart34, &cart34_rank);

        int coords[2];
        MPI_Cart_coords(cart34, cart34_rank, 2, coords);

        printf("World rank %02d: cart34 size is %d, cart34 rank is %d with coordinates (%d, %d)\n", 
               world_rank, cart34_size, cart34_rank, coords[0], coords[1]);
    }

    if(atoi(argv[1]) == 5){
        MPI_Comm square_base;

        int participating = world_rank < 9;  // get a square number of processors

        MPI_Comm_split(MPI_COMM_WORLD, participating, 0, &square_base);

        int participating_size, participating_rank;
        MPI_Comm_size(square_base, &participating_size);
        MPI_Comm_rank(square_base, &participating_rank);

        if(participating){

            MPI_Comm square;

            int ndims = 2;
            int reorder = 0; // can choose to re-order the ranks
            
            int dims[2] = {3, 3};  // for 2D, rows X columns
            int periods[2] = {0, 0};  // is the decomposition periodic
            
            MPI_Cart_create(square_base, ndims, dims, periods, reorder, &square);

            int square_size, square_rank;
            MPI_Comm_size(square, &square_size);
            MPI_Comm_rank(square, &square_rank);

            int coords[2];
            MPI_Cart_coords(square, square_rank, 2, coords);

            printf(
                "World rank %02d: participating size is %d and participating rank is %d, "
                "square size is %d, square rank is %d with coordinates (%d, %d)\n", 
                   world_rank, participating_size, participating_rank, 
                   square_size, square_rank, coords[0], coords[1]);

        } else {
            printf("World rank %02d: not participating size is %d and not participating rank is %d\n", 
                   world_rank, participating_size, participating_rank);
        }
    }


    MPI_Finalize();

    return 0;
}