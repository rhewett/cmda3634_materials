#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    int rank, n_procs;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_procs);

    MPI_Status status;

    int token = 0;
    int start_rank = n_procs-1;
    int dest_rank = (rank + 1)%n_procs;
    int src_rank = (rank - 1)%n_procs;

    if (rank == start_rank)
        MPI_Send(&token, 1, MPI_INT, dest_rank, 0, MPI_COMM_WORLD);

    MPI_Recv(&token, 1, MPI_INT, src_rank, 0, MPI_COMM_WORLD, &status);
    token += 1;

    if (rank != start_rank)
        MPI_Send(&token, 1, MPI_INT, dest_rank, 0, MPI_COMM_WORLD);

    printf("When rank %d saw the token, it had been incremented %d times.\n", rank, token);

    MPI_Finalize();
}
