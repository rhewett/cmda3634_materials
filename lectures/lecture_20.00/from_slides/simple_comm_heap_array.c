#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Status status;

    int* send_data = malloc(N*sizeof(int));
    int* recv_data = malloc(N*sizeof(int));

    if (rank == 0) initialize_data(send_data, N);
    if (rank == 0) nullify_data(recv_data, N);

    if (rank == 1) nullify_data(send_data, N);
    if (rank == 1) nullify_data(recv_data, N);

    if(rank == 0) MPI_Send(send_data, N, MPI_INT, 1, tag, MPI_COMM_WORLD);
    if(rank == 1) MPI_Recv(recv_data, N, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

    MPI_Finalize();
}
