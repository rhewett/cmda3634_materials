#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Status status;

    int send_data = -1;
    int recv_data = -1;
    if (rank == 0) send_data = 124344;

    printf("a) Before send: Rank %d send=%d recv=%d\n", rank, send_data, recv_data);

    MPI_Send(&send_data, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
    MPI_Recv(&recv_data, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

    printf("b) After send: Rank %d send=%d recv=%d\n", rank, send_data, recv_data);

    MPI_Finalize();
}
