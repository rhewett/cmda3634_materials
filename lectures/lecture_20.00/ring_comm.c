#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    int rank, n_procs;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_procs);

    int token = 0;

    printf("When rank %d saw the token, it had been incremented %d times.\n", rank, token);

    MPI_Finalize();
}
