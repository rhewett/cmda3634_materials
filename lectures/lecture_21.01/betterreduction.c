#include <stdio.h>
#include <math.h>
#include <mpi.h>

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);

    int tag = 0;
    int rank, n_procs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_procs);
    MPI_Status status;

    int data = rank+1; // compute interesting data
    int reduced_data = data;



    printf("Rank %d: Reduction value = %d.\n", rank, reduced_data);
    MPI_Finalize();
}