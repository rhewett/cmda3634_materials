#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

void print_array(int* arr, int n, int p, int n_procs, char* prefix){

    for(int j=0; j<n_procs; j++){
        if (j==p){
            if (p == 0) fprintf(stdout, "\n");
            fprintf(stdout, "%s %d: ", prefix, p);
            for(int i=0; i<n; i++){
                fprintf(stdout, "%d ", arr[i]);
            }
            fprintf(stdout, "\n");
            fflush(stdout);
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

}

void exchange_halo_data(int* x, int p, int n_procs, int Mp, int q){
    // Each rank has up to two sends and two receives
    MPI_Status statuses[4];
    MPI_Request requests[4] = {MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL};
    if(n_procs == 1) return;

    // receive from up / left
    if(p > 0){
        MPI_Irecv(x, q, MPI_INT, p-1, 0, MPI_COMM_WORLD, &requests[0]);
    }
    // receive from down / right
    if(p < n_procs-1){
        MPI_Irecv(x+Mp+q, q, MPI_INT, p+1, 1, MPI_COMM_WORLD, &requests[1]);
    }
    // send down / right
    if(p < n_procs-1){
        MPI_Isend(x+Mp, q, MPI_INT, p+1, 0, MPI_COMM_WORLD, &requests[2]);
    }
    // send up / left
    if(p > 0){
        MPI_Isend(x+q, q, MPI_INT, p-1, 1, MPI_COMM_WORLD, &requests[3]);
    }

    MPI_Waitall(4, requests, statuses);
}

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);
    int p;  MPI_Comm_rank(MPI_COMM_WORLD, &p);
    int n_procs;  MPI_Comm_size(MPI_COMM_WORLD, &n_procs);

    int M = atoi(argv[1]);
    int K = atoi(argv[2]);
    int q = (K-1)/2;
    int Mp = M/n_procs;

    int* x_p = malloc((Mp+2*q)*sizeof(float));
    int* y_p = malloc((Mp+2*q)*sizeof(float));

    for(int i=0; i<(Mp+2*q); i++){
        x_p[i] = 0;
        y_p[i] = -1;
    }

    int start_idx = q;
    int stop_idx = q+Mp;

    for(int i=start_idx; i<stop_idx; i++){
        x_p[i] = p;
        y_p[i] = 0;
    }

    print_array(x_p, Mp+2*q, p, n_procs, "a) init  x ");

    exchange_halo_data(x_p, p, n_procs, Mp, q);

    print_array(x_p, Mp+2*q, p, n_procs, "b) halo x ");

    for(int i=start_idx; i<stop_idx; i++){
        y_p[i] = 0;
        for(int j=0; j<K; j++){
            y_p[i] += x_p[i-q+j];
        }
    }

    print_array(y_p, Mp+2*q, p, n_procs, "c) final y ");

    free(x_p);
    free(y_p);

    MPI_Finalize();
}