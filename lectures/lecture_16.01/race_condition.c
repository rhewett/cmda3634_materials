#include <omp.h>
#include <stdio.h>

int main(int argc, char **argv) {
    omp_set_num_threads(4);

    int thread;

#pragma omp parallel shared(thread)
{
    thread = omp_get_thread_num();
}
    printf("%d\n", thread);
    return 0;
}

