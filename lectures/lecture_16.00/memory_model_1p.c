#include <omp.h>
#include <stdio.h>

int main(int argc, char **argv) {
    omp_set_num_threads(4);

    int data[4];

    printf("Initial value [%d, %d, %d, %d]\n", data[0], data[1], data[2], data[3]);

#pragma omp parallel private(data)
{
    int thread = omp_get_thread_num();
    data[thread] = thread;

    printf("Thread %d sees [%d, %d, %d, %d]\n", thread, data[0], data[1], data[2], data[3]);
}

    printf("Final value [%d, %d, %d, %d]\n", data[0], data[1], data[2], data[3]);
    return 0;
}
