#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    int rank, n_procs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_procs);

    int n_global = atoi(argv[1]);
    float* global_data;
    if(rank == 0){
        global_data = (float*) malloc(n_global*sizeof(float));
        for(int i=0; i<n_global; i++)
            global_data[i] = 0.5;

        printf("Before: \n");
        for(int i=0; i<n_global; i++)
            printf("%f,\t", global_data[i]);
        printf("\n");
    }

    // Global data has been initialized
    int n_local = n_global / n_procs;
    float* local_data = (float*) malloc(n_local*sizeof(float));

    MPI_Scatter(global_data, n_local, MPI_FLOAT,
                local_data,  n_local, MPI_FLOAT,
                0, MPI_COMM_WORLD);

    for(int i=0; i<n_local; i++)
        local_data[i] += rank;

    MPI_Gather(local_data,  n_local, MPI_FLOAT,
               global_data, n_local, MPI_FLOAT,
               0, MPI_COMM_WORLD);

    free(local_data);

    if(rank == 0){
        printf("After: \n");
        for(int i=0; i<n_global; i++)
            printf("%f,\t", global_data[i]);
        printf("\n");

        free(global_data);
    }

    MPI_Finalize();
}
