#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){

    printf("argc: %d\n", argc);
    printf("argv[0]: %s\n", argv[0]);

    int size = atoi(argv[1]);

    int* heap_array = NULL;

    heap_array = malloc(size*sizeof(int));

    if (heap_array == NULL){
        printf("allocation failed!");
        exit(1);
    }

    for (int i=0; i<size; i++)
        heap_array[i] = i;

    for (int i=0; i<size; i+=5)
        printf("%d\n", heap_array[i]);

    free(heap_array);

}
