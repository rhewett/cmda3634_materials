#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    int n_data = atoi(argv[1]);
    int* data = malloc(n_data*sizeof(int));
    FILE* f;

    int rank, size;
    MPI_Status status;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    for(int i=0; i<n_data; i++){
        int v = rank*n_data + i;
        data[i] = v*v;
    }

    if (rank == 0){
        f = fopen(argv[2], "wb");

        int n_written_data = fwrite(data, sizeof(int), n_data, f);
        printf("Wrote %d data from rank 0!\n", n_written_data);

        for(int p=1; p<size; p++){
            MPI_Recv(data, n_data, MPI_INT, p,  0,  MPI_COMM_WORLD, &status);
            n_written_data = fwrite(data, sizeof(int), n_data, f);

            printf("Wrote %d data from rank %d!\n", n_written_data, p);
        }

        fclose(f);
    }
    else{
        MPI_Send(data, n_data, MPI_INT, 0,  0,  MPI_COMM_WORLD);
    }

    free(data);
    MPI_Finalize();
}
