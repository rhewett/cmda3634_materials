#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    int n_data = atoi(argv[1]);
    int* data = malloc(n_data*sizeof(int));

    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_File f;
    MPI_Status status;
    MPI_Offset offset;

    // Header sise *in bytes*
    int header_size = 0;
    int n_read_data;

    MPI_File_open(MPI_COMM_WORLD, argv[2], MPI_MODE_RDONLY, MPI_INFO_NULL, &f);

    offset = header_size + rank*n_data*sizeof(int);

    MPI_File_read_at_all(f, offset, data, n_data, MPI_INT, &status);

    MPI_Get_count(&status, MPI_INT, &n_read_data);

    MPI_File_close(&f);

    printf("Rank %d read %d data!\n", rank, n_read_data);

    for (int p=0; p<size; p++){
        if (p == rank){
            for(int i=0; i<n_data; i++){
                printf("%d ", data[i]);
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    free(data);

    MPI_Finalize();
}
