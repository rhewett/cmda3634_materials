#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);

    int tag = 0;
    int rank, n_procs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_procs);
    MPI_Status status;

    int root_rank = 0;

    int data = rank+1; // compute interesting data
    int product;

    // reduce to root rank & broadcast the result
    MPI_Reduce(&data, &product, 1, MPI_INT, MPI_PROD, root_rank, MPI_COMM_WORLD);

    MPI_Bcast(&product, 1, MPI_INT, root_rank, MPI_COMM_WORLD);

    printf("Rank %d: Reduction value = %d.\n", rank, product);
    MPI_Finalize();
}