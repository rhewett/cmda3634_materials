#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);

    int tag = 0;
    int rank, n_procs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_procs);
    MPI_Status status;

    int data = -1; // compute interesting data
    printf("Rank %d: Pre-roadcast value = %d.\n", rank, data);

    if (rank == 0){
        data = 7;
        for(int i=1; i<n_procs; i++){
            MPI_Send(&data, 1, MPI_INT, i, tag, MPI_COMM_WORLD);
        }
    }
    else{
        MPI_Recv(&data, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);
    }
    printf("Rank %d: Broadcast value = %d.\n", rank, data);
    MPI_Finalize();
}